'use strict';

/*
 Use CustomersController and CustomerController as your base functions.

 All controllers goes in this file.

 In your controllers, order your code the following way:
 1. set variables
 2. call init();
 3. functions
 4. watchers
 5. init function for loading data and setup
 */

angular.module('dealerPortal')
    .controller('ContactController', function ContactController () {
        return this;
    })

    .controller('CustomerController', function CustomerController ($scope, $state, $stateParams, PortalData, Modal, Messaging, Data, Static) {
        init();

        $scope.new = function() {
            if ($scope.form.$pristine) {
                $state.go('customer',{'id': -1});
            }
            else {
                // If the user selects ignore, this value is used.
                $scope.destination = 'customer';

                var modalInstance = Modal.unsaved($scope);
                modalInstance.result.then(function () {
                    Data.save('customers', $scope);
                    $state.go('customer',{'id': -1});
                });
            }
        };

        $scope.save = function () {
            if ($scope.form.$valid) {
                $scope.destination = 'customer';
                Data.save('customers', $scope);
            }
            else {
                Messaging.warningMessage('Please complete the form');
            }
        };

        $scope.saveClose = function() {
            if ($scope.form.$valid) {
                $scope.destination = 'customers';
                Data.save('customers', $scope);
            }
            else {
                Messaging.warningMessage('Please complete the form');
            }
        };

        $scope.close = function () {
            if ($scope.form.$pristine) {
                $state.go('customers');
            }
            else {
                $scope.destination = 'customers';
                var modalInstance = Modal.unsaved($scope);
                modalInstance.result.then(function () {
                    Data.save('customers', $scope);
                });
            }
        };

        $scope.stateByCountry = function (countryId) {
            // Filters state dropdown based on country. Item is each element in the state array.
            return function (item) {
                return item.countryId === countryId;
            };
        };

        function init() {
            // States List
            Static.states('').success(function (data, status) {
                $scope.states = data;
            });

            // Countries List
            Static.countries().success(function (data, status) {
                $scope.countries = data;
            });

            // New customer
            if (parseInt($stateParams.id) === -1) {
                $scope.data = {
                    'dealerId': dealerId
                };
            }
            else {
                // Existing customer. Uses $q to handle resolved promise.
                Data.loadItem('customers', $stateParams.id).then(function (data) {
                    $scope.data = data;

                    // Splice in notification types that aren't on the user
                    Static.notificationTypes().success(function (result) {
                        var key, i, j;

                        // Remove notification types that the user has from the notifications array
                        for (i = 0; i < $scope.data.userNotifications.length; i++) {
                            for (key in result) {
                                if ($scope.data.userNotifications[i].notificationTypeId === result[key].id) {
                                    result.splice(key, 1);
                                    key = 0;
                                }
                            }
                        }

                        // Splice notifications array with user's notifications
                        for (j = 0; j < result.length; j++) {
                            $scope.data.userNotifications.push({
                                userId: data.id,
                                notificationTypeId: result[j].id,
                                notificationType: {
                                    notificationDesc: result[j].notificationDesc,
                                    notificationCode: result[j].notificationCode
                                }
                            });
                        }
                    });

                    // Get the user's contracts
                    PortalData.readSubElement('customers', $stateParams.id, 'contracts').success(function (data, status) {
                        $scope.contracts = data;
                    }).error(function (error, status) {
                        Messaging.error (status);
                    });

                    // Get the user's claims
                    PortalData.readSubElement('customers', $stateParams.id, 'claims').success(function (data, status) {
                        $scope.claims = data;
                    }).error(function (error, status) {
                        Messaging.error (status);
                    });

                });
            }
        }
    })

    .controller('CustomersController', function CustomersController ($scope, Data, Modal, PortalData, Static, Messaging, filterFilter) {
        var ready = 0;
        init();

        $scope.$watch('search', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        $scope.resetForm = function (value) {
            dealerPortal.resetForm($scope, value);
        };

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.deleteCustomer = function (id) {
            // Modal dialog for OK/Cancel
            var modalInstance = Modal.deleteCustomer($scope);

            // Executed on OK in modal
            modalInstance.result.then(function () {
                PortalData.delete('customers', id).success(function (data, status) {
                    Messaging.successMessage('The user has been deleted');
                    init();
                }).error( function (error, status){
                    Messaging.errorObject(error);

                });
            });
        };


        function init() {
            Static.states('').success(function (data, status) {
                $scope.states = data;
            });

            Data.loadList('customers').then(function (data) {
                $scope.data = data;
                ready = 1;
                dealerPortal.filterlist($scope, filterFilter, ready);
            });

            dealerPortal.fieldSet($scope, 'search', 'customersForm.search');
        }
    })

    .controller('ContractController', function ContractController ($scope, $state, $stateParams, Data, Messaging, PortalData, Static) {
        // Date options for pop-up calendar
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            'show-weeks': false
        };

        init();

        $scope.stateByCountry = function (countryId) {
            // Item is each element in the state array.
            return function (item) {
                return item.countryId === countryId;
            };
        };

        function init() {
            // States List
            Static.states('').success(function (data, status) {
                $scope.states = data;
            });

            // Countries List
            Static.countries().success(function (data, status) {
                $scope.countries = data;
            });

            // New Contract
            if (parseInt($stateParams.id) === -1) {
                $scope.data = {};
            }
            // Existing Contract
            else {
                Data.loadItem('contracts', $stateParams.id).then(function (data) {
                    $scope.data = data;
                });
            }
        }
    })

    .controller('ContractsController', function ContractsController ($scope, $stateParams, Data, Messaging, PortalData, filterFilter) {
        var ready = 0;
        var fileId = $stateParams.id;
        init();

        $scope.resetForm = function (value) {
            dealerPortal.resetForm($scope, value);
        };

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.$watch('search', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        $scope.$watch('currentPage', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        function init() {
            //TODO Use the file id as a filter for the list
            if (fileId < 1) {
                fileId = -1;
            }

            Data.loadList('contracts').then(function (data) {
                $scope.data = data;
                ready = 1;
                dealerPortal.filterlist($scope, filterFilter, ready);
            });

            dealerPortal.fieldSet($scope, 'search', 'contractsForm.search');
        }
    })

    .controller('FileController', function FileController ($scope) {

    })

    .controller('FilesController', function FilesController ($scope) {
        init();

        $scope.resetForm = function (value) {
            dealerPortal.resetForm($scope, value);
        };

        function init() {
            dealerPortal.dropDownSetValue($scope, 'year', 'filesForm.year');
            dealerPortal.dropDownSetValue($scope, 'month', 'filesForm.month');
            dealerPortal.fieldSet($scope, 'search', 'filesForm.search');
        }
    })

    .controller('FileUploadController', function FileUploadController ($scope, FileUploader) {
        /*
         var uploader = $scope.uploader = new FileUploader({
         scope: $scope,
         url: '/api/fileUpload'
         });
         */
    })

    .controller('LoginController', function LoginController ($scope, Auth) {
        $scope.loginDealer = {
            email: '',
            password: ''
        };

        $scope.loginDealerAction = function () {
            if ($scope.loginDealerForm.validate().$valid || $scope.testing) {

                Auth.login($scope.loginDealer.email, $scope.loginDealer.password).then(function (user) {
                    // Todo
                });
            }
        };
    })

    .controller('MainController', function MainController ($scope, $state) {
        // Sets up menu highlighting
        $scope.$state = $state;
    })

    .controller('ResourcesController', function ResourcesController () {
        return this;
    })

    .controller('SaleController', function SaleController ($scope, $state, $stateParams, $modal, Data, Modal, PortalData, Messaging, Static) {
        // Date options for pop-up calendar
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            'show-weeks': false
        };

        init();

/*
        $scope.save = function () {
            Data.save('sales', $scope).then(function () {
                $state.go('salesProcessed');
            });
        };
*/

        $scope.addSaleProduct = function () {
            // Creates a new products array for new sale
            if (angular.isObject($scope.data.saleProducts) === false) {
                $scope.data.saleProducts = [];
            }

            // Adds a blank product to products
            $scope.data.saleProducts.push({});
        };


        $scope.removeSaleProduct = function (id) {
            // Modal dialog for OK/Cancel
            var modalInstance = Modal.deleteSale($scope);

            // Executed on OK in modal
            modalInstance.result.then(function () {
        
                Messaging.successMessage('The sale has been deleted');
            });
        };

        $scope.stateByCountry = function (countryId) {
            // Filters the state list for the selected country
            return function (item) {
                return item.countryId === countryId;
            };
        };

        function init() {
            // Warranties List
            Static.warrantySkus().success(function (data, status) {
                $scope.warrantySkus = data;
            });

            // Additional Warranties List
            Static.warrantySkusAdditional().success(function (data, status) {
                $scope.warrantySkusAdditional = data;
            });

            // States List
            Static.states('').success(function (data, status) {
                $scope.states = data;
            });

            // Countries List
            Static.countries().success(function (data, status) {
                $scope.countries = data;
            });

            // Categories List
            Static.categories().success(function (data, status) {
                $scope.categories = data;
            });

            // Product Types List
            Static.productTypes().success(function (data, status) {
                $scope.productTypes = data;
            });

            // Manufacturers List
            Static.manufacturers().success(function (data, status) {
                $scope.manufacturers = data;
            });

            // New sale
            if (parseInt($stateParams.id) === -1) {
                $scope.data = {};
            }
            // Read sale
            else {

                Data.loadItem('sales', $stateParams.id).then(function (data) {
                    $scope.data = data;
                });
            }
        }
    })

    .controller('SalesController', function SalesController ($scope, $stateParams, Data, Messaging, PortalData, filterFilter) {
        var ready = 0;
        var fileId = $stateParams.id;
        init();

        $scope.resetForm = function resetForm (value) {
            dealerPortal.resetForm($scope, value);
        };

        $scope.setPage = function setPage (pageNo) {
            $scope.currentPage = pageNo;
        };

        // Watches filters the current list of sales based on input fields
        $scope.$watch('search', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        $scope.$watch('month', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        $scope.$watch('year', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        $scope.$watch('currentPage', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        function init () {
            // Todo Filter list on file id.
            if (fileId < 1) {
                fileId = -1;
            }

            Data.loadList('sales').then(function (data) {
                $scope.data = data;
                ready = 1;
                dealerPortal.filterlist($scope, filterFilter, ready);
            });


            // Sets the filters to vales from Local Storage
            dealerPortal.dropDownSetValue($scope, 'year', 'salesForm.year');
            dealerPortal.dropDownSetValue($scope, 'month', 'salesForm.month');
            dealerPortal.fieldSet($scope, 'search', 'salesForm.search');
        }
    })

    .controller('SalesErrorsController', function SalesErrorsController ($scope, $stateParams, Data, Messaging, PortalData, filterFilter) {
        var ready = 0;
        var fileId = $stateParams.id;
        init();

        $scope.resetForm = function (value) {
            dealerPortal.resetForm($scope, value);
        };

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.$watch('search', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        $scope.$watch('month', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        $scope.$watch('year', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        $scope.$watch('currentPage', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        function init() {
            // Todo If no file has been selected, show data from all files
            if (fileId < 1) {
                fileId = -1;
            }

            Data.loadList('sales').then(function (data) {
                $scope.data = data;
                ready = 1;
                dealerPortal.filterlist($scope, filterFilter, ready);
            });

            dealerPortal.dropDownSetValue($scope, 'year', 'salesForm.year');
            dealerPortal.dropDownSetValue($scope, 'month', 'salesForm.month');
            dealerPortal.fieldSet($scope, 'search', 'salesForm.search');
        }
    })

    .controller('SalesProcessedController', function SalesProcessedController ($scope, $stateParams, Data, PortalData, filterFilter, Messaging) {
        var ready = 0;
        var fileId = $stateParams.id;
        init();

        $scope.resetForm = function (value) {
            dealerPortal.resetForm($scope, value);
        };

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.$watch('search', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        $scope.$watch('month', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        $scope.$watch('year', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        $scope.$watch('currentPage', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        function init() {
            if (fileId < 1) {
                fileId = -1;
            }

            // Todo Complete this
            Data.loadList('sales').then(function (data) {
                $scope.data = data;
                ready = 1;
                dealerPortal.filterlist($scope, filterFilter, ready);
            });

            dealerPortal.dropDownSetValue($scope, 'year', 'salesForm.year');
            dealerPortal.dropDownSetValue($scope, 'month', 'salesForm.month');
            dealerPortal.fieldSet($scope, 'search', 'salesForm.search');
        }
    })

    .controller('StoreController', function StoreController () {
        return this;
    })

    .controller('StoresController', function StoresController ($scope) {
        init();

        $scope.resetForm = function (value) {
            dealerPortal.resetForm($scope, value);
        };

        function init() {
            dealerPortal.fieldSet($scope, 'search', 'storesForm.search');
        }
    })

    .controller('UserController', function UserController ($scope, $state, $stateParams, Data, PortalData, Messaging) {
        init();

        // New users must have a password. Function is called by ng-require on the field
        $scope.requirePassword = function requirePassword () {
            if (typeof $scope.data !== 'undefined' && typeof $scope.data.$id === 'string') {
                return false;
            }
            else {
                return true;
            }
        };

        function init() {
            if (parseInt($stateParams.id) === -1) {
                $scope.data = {'dealerId': dealerId};
            }
            // Existing User
            else {
                // encodeURIComponent is used since the url contains @
                PortalData.read('users', encodeURIComponent($stateParams.id)).success(function (data, status) {
                    $scope.data = data;
                }).error (function (error, status) {
                    Messaging.error (status);
                });
            }
        }
    })

    .controller('UsersController', function UsersController ($scope, $modal, $stateParams, Data, Modal, PortalData, Messaging, filterFilter) {
        var fileId = 0;
        var ready = 0;
        init();

        $scope.resetForm = function (value) {
            dealerPortal.resetForm($scope, value);
        };

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.$watch('search', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        $scope.$watch('currentPage', function () {
            dealerPortal.filterlist($scope, filterFilter, ready);
        });

        $scope.deleteUser = function (emailAddress) {
            // Modal dialog for OK/Cancel
            var modalInstance = Modal.deleteUser($scope);

            // Executed on OK in modal
            modalInstance.result.then(function () {
                PortalData.delete('users', encodeURIComponent(emailAddress)).success(function (data, status) {
                    Messaging.successMessage('The user has been deleted');
                }).error( function (error, status){
                    Messaging.errorObject(error);

                });
            });
        };

        function init() {
            if (fileId < 1) {
                fileId = -1;
            }

            Data.loadList('users').then(function (data) {
                $scope.data = data;
                ready = 1;
                dealerPortal.filterlist($scope, filterFilter, ready);
            });

            dealerPortal.fieldSet($scope, 'search', 'usersForm.search');
        }
    })

    .controller('modalSaveConfirm', function($state, $scope, $modalInstance, destination) {
        $scope.cancel= function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.ignore= function($scope) {
            $modalInstance.dismiss('cancel');

            if (destination) {
                $state.go(destination, {'id': -1});
            }
        };

        $scope.save = function (){
            $modalInstance.close();
        };
    })

    .controller('modalOkCancel', function($state, $scope, $modalInstance) {
        $scope.cancel= function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.ok = function (){
            $modalInstance.close();
        };
    })
;
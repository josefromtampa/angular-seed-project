'use strict';

// All services and factories goes in this file

angular.module('dealerPortal')
    // Login and logout functions
    .factory('Auth', function authFactory ($http) {
        return {
            login: function loginFactory (formdata) {
                return $http.post(server + baseUrl + '/dealer/' + dealerId + '/login', formdata);
            },

            logout: function logoutFactory () {
                return $http.delete(server + baseUrl + '/dealer/' + dealerId + '/login');
            }
        };
    })

    // Data functions. Uses PortalData for http operations.
    // When you use the notation format, all elements must be present and in the same order.
    .factory('Data', ['Messaging', 'PortalData', '$state', '$q', function dataFactory (Messaging, PortalData, $state, $q) {
        return {
            // Uses $scope.destination to reinitialize page or redirect.
            save: function saveFactory (dataType, $scope) {
                if ($scope.form && $scope.form.$dirty) {
                    if (!$scope.data.id) {
                        PortalData.create(dataType, $scope.data)
                            .success(function (data, status) {
                                Messaging.successSave();

                                if ($scope.destination) {
                                    $state.go($scope.destination,{'id': -1});
                                }
                            })
                            .error(function (error, status) {
                                Messaging.error(status, dataType);
                            });
                    }
                    else {
                        PortalData.update(dataType, $scope.data.id, $scope.data)
                            .success(function (data, status) {
                                Messaging.successSave();
                                if ($scope.destination) {
                                    $state.go($scope.destination,{'id': -1});
                                }
                            })
                            .error(function (error, status) {
                                Messaging.error(status, dataType);
                            });
                    }
                }
                else {
                    // We only save if there are changes
                    if ($scope.destination) {
                        $state.go($scope.destination,{'id': -1});
                    }
                }
            },

            loadItem: function loadItemFactory (dataType, id) {
                var deferred = $q.defer();

                PortalData.read(dataType, id)
                    .success(function (data, status) {
                        deferred.resolve(data);
                        return deferred.promise;

                    })
                    .error(function (error, status) {
                        Messaging.errorObject(error);
                        return deferred.promise;
                    });

                return deferred.promise;
            },

            loadList: function loadListFactory (dataType) {
                var deferred = $q.defer();

                PortalData.list(dataType)
                    .success(function (data, status) {
                        deferred.resolve(data);
                        return deferred.promise;
                    })
                    .error(function (error, status) {
                        Messaging.errorObject(error);
                        return deferred.promise;
                    });

                return deferred.promise;
            }
        };
    }])

    // Used for all CRUD operations
    .factory('PortalData', function portalDataFactory ($http) {
        return {
            list: function listFactory (dataType) {
                return $http.get(server + baseUrl + '/dealer/' + dealerId + '/' + dataType);
            },

            listFiltered: function listFilteredFactory (dataType, filter) {
                return $http.get(server + baseUrl + '/dealer/' + dealerId + '/' + dataType + '/' + filter);
            },

            create: function createFactory (dataType, formdata) {
                return $http.post(server + baseUrl + '/dealer/' + dealerId + '/' + dataType + '/', formdata);
            },

            read: function readFactory (dataType, id) {
                return $http.get(server + baseUrl + '/dealer/' + dealerId + '/' + dataType + '/' + id);
            },

            readSubElement: function readFactory (dataType, id, subElement) {
                return $http.get(server + baseUrl + '/dealer/' + dealerId + '/' + dataType + '/' + id + '/' + subElement);
            },

            update: function updateFactory (dataType, id, formdata) {
                return $http.put(server + baseUrl + '/dealer/' + dealerId + '/' + dataType + '/' + id, formdata);
            },

            delete: function deleteFactory (dataType, id) {
                return $http.delete(server + baseUrl + '/dealer/' + dealerId + '/' + dataType + '/' + id);
            }
        };
    })

    // Values for drop-downs
    .factory('Static', function staticFactory ($http) {
        return {
            states: function statesFactory (country) {
                return $http.get(server + baseUrl + '/states?country=' + country);
            },

            countries: function countriesFactory () {
                return $http.get(server + baseUrl + '/countries');
            },

            notificationTypes: function notificationTypesFactory () {
                return $http.get(server + baseUrl + '/notificationtypes');
            },

            productTypes: function productTypesFactory () {
                return $http.get(server + baseUrl + '/dealer/' + dealerId + '/producttypes');
            },

            warrantySkus: function warrantySkusFactory () {
                return $http.get(server + baseUrl + '/dealer/' + dealerId + '/warrantyskus');
            },

            warrantySkusAdditional: function warrantySkusAdditionalFactory () {
                return $http.get(server + baseUrl + '/dealer/' + dealerId + '/warrantyskusadditional');
            },

            manufacturers: function manufacturersFactory () {
                return $http.get(server + baseUrl + '/dealer/' + dealerId + '/manufacturers');
            },

            categories: function categoriesFactory () {
                return $http.get(server + baseUrl + '/dealer/' + dealerId + '/categories');
            }
        };
    })

    // Modal window function
    .factory('Modal', function messagingFactory ($modal) {
        return {
            // Unsaved Data. Needs $scope.destination to redirect.
            unsaved: function unsaved ($scope) {
                return $modal.open({
                    templateUrl: '/scripts/dealer/views/modalSaveConfirm.html',
                    controller: 'modalSaveConfirm',
                    resolve: {
                        destination: function () {
                            return $scope.destination;
                        }
                    }
                });
            },
            deleteCustomer: function deleteCustomer ($scope) {
                return $modal.open({
                    templateUrl: '/scripts/dealer/views/modalDeleteCustomer.html',
                    controller: 'modalOkCancel'
                });
            },
            deleteSale: function deleteSale ($scope) {
                return $modal.open({
                    templateUrl: '/scripts/dealer/views/modalDeleteSale.html',
                    controller: 'modalOkCancel'
                });
            },
            deleteUser: function deleteUser ($scope) {
                return $modal.open({
                    templateUrl: '/scripts/dealer/views/modalDeleteUser.html',
                    controller: 'modalOkCancel'
                });
            }


        };
    })

    // Messaging service.
    .factory('Messaging', ['toaster', function messagingFactory (toaster) {
        return {
            warningMessage: function warningMessage (message, title) {
                if (!title) {
                    title = 'Warning!';
                }

                if (!message) {
                    message = 'We forgot to put in the warning message';
                }

                toaster.pop('warning', title, message, 5000, 'trustedHtml');
            },
            // Error message function
            errorMessage: function errorMessage (message, title) {
                if (!title) {
                    title = 'Something went wrong';
                }

                if (!message) {
                    message = 'We forgot to put in the error message';
                }

                toaster.pop('error', title, message, 5000, 'trustedHtml');
            },

            // Default error function
            error: function error (status, dataType, message, title) {
                if (!title) {
                    title = 'Something went wrong';
                }

                if (!message) {
                    message = 'The server responded with error code ' + status;
                }

                if (dataType && dataType === 'customers' && status === 403) {
                    message = 'Primary e-mail address already exists';
                }

                toaster.pop('error', title, message, 5000, 'trustedHtml');
            },

            // Pass in the error object and unpack all messages
            errorObject: function errorObject (err, title) {
                var errorTitle = '';
                var errorText = '';

                if (title) {
                    errorTitle = title;
                }
                else if (err.message) {
                    errorTitle = err.message;
                }
                else {
                    errorTitle = 'Error';
                }

                for (var key in err) {
                    if (key === 'modelState') {
                        var obj = err[key];
                        for (var prop in obj) {
                            // hasOwnProperty check is to not output prototype inherited properties
                            if (obj.hasOwnProperty(prop) && typeof(obj) === 'object' && prop !== '$id') {
                                errorText += obj[prop] + '<br/>';
                            }
                        }
                    }
                }

                toaster.pop('error', errorTitle, errorText, 5000, 'trustedHtml');
            },

            successMessage: function successMessage (message, title) {
                var successTitle = '';

                if (title) {
                    successTitle = title;
                }
                else {
                    successTitle = 'Success';
                }

                toaster.pop('success', successTitle, message, 5000, 'trustedHtml');
            },

            successSave: function successSaveMessage() {
                toaster.pop('success', 'Success', 'Your data has been saved', 5000, 'trustedHtml');
            }
        };
    }])

    // HTTP interceptor that we use to enable and disable please wait overlay using $rootScope.waiting.
    .factory('httpInterceptor', ['$q', '$rootScope', '$injector',
        function ($q, $rootScope, $injector) {
            $rootScope.waiting = false;
            $rootScope.http = null;
            return {
                'request': function (config) {
                    $rootScope.waiting = true;
                    return config || $q.when(config);
                },
                'requestError': function (rejection) {
                    $rootScope.http = $rootScope.http || $injector.get('$http');
                    if ($rootScope.http.pendingRequests.length < 1) {
                        $rootScope.waiting = false;
                    }
                    /*
                     if (canRecover(rejection)) {
                     return responseOrNewPromise
                     }
                     */
                    return $q.reject(rejection);
                },
                'response': function (response) {
                    $rootScope.http = $rootScope.http || $injector.get('$http');
                    if ($rootScope.http.pendingRequests.length < 1) {
                        $rootScope.waiting = false;
                    }
                    return response || $q.when(response);
                },
                'responseError': function (rejection) {
                    $rootScope.http = $rootScope.http || $injector.get('$http');
                    if ($rootScope.http.pendingRequests.length < 1) {
                        $rootScope.waiting = false;
                    }
                    /*
                     if (canRecover(rejection)) {
                     return responseOrNewPromise
                     }
                     */
                    return $q.reject(rejection);
                }
            };
        }
    ])

;

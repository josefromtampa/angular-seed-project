'use strict';

// All shared functions go in this file.
var dealerPortal = {};

// Reads local storage value and sets the drop-down based on the item text
dealerPortal.dropDownSetText = function dropDownSetText ($scope, model, element) {
    var dd = document.getElementById(element);
    dd.onchange = function () {
        localStorage.setItem(element, dd.options[dd.selectedIndex].text);
    };

    if (localStorage.getItem(element)) {
        $scope[model] = localStorage.getItem(element);
    }
};

// Reads local storage value and sets the drop-down based on the item id
dealerPortal.dropDownSetValue = function dropDownSetValue ($scope, model, element) {
    var dd = document.getElementById(element);
    dd.onchange = function () {
        localStorage.setItem(element, dd.options[dd.selectedIndex].value);
    };

    if (localStorage.getItem(element)) {
        $scope[model] = localStorage.getItem(element);
    }
};

// Reads local storage value and sets input field
dealerPortal.fieldSet = function fieldSet ($scope, model, element) {
    var dd = document.getElementById(element);
    dd.onchange = function () {
        localStorage.setItem(element, dd.value);
    };

    if (localStorage.getItem(element)) {
        $scope[model] = localStorage.getItem(element);
    }
};

// Blanks all fields in the form
dealerPortal.resetForm = function resetForm ($scope, formName) {
    if ($scope[formName].$dirty) {
        $scope[formName].$setPristine();
    }

    if ($scope.month) {
        $scope.month = '';
        localStorage.removeItem(formName + '.month');
    }

    if ($scope.search) {
        $scope.search = '';
        localStorage.removeItem(formName + '.search');
    }

    if ($scope.year) {
        $scope.year = '';
        localStorage.removeItem(formName + '.year');
    }
};

// Filters list on input fields
dealerPortal.filterlist = function filterList ($scope, filterFilter, ready) {
    if ($scope.data) {
        if (!$scope.pageSize) {
            $scope.pageSize = 10;
        }
        if (!$scope.currentPage) {
            $scope.currentPage = 1;
        }
        if ($scope.data && ready === 1) {
            $scope.startFrom = ($scope.currentPage - 1) * $scope.pageSize;
            $scope.filteredData = filterFilter(filterFilter(filterFilter($scope.data, $scope.search), $scope.year), $scope.month);
            $scope.totalItems = filterFilter(filterFilter(filterFilter($scope.data, $scope.search), $scope.year), $scope.month).length;
        }
    }
};


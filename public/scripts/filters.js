'use strict';

// All filters goes in this file

angular.module('dealerPortal')
    // Used by list paginator
    .filter('startFrom', function startFromFilter () {
        return function (input, start) {
            if (input) {
                start = +start; //parse to int
                return input.slice(start);
            }
            return [];
        };
    })

    // Formats output to Title Case
    .filter('titleCase', function titleCaseFilter () {
        return function (str) {
            return (str === undefined || str === null) ? '' : str.replace(/_|-/, ' ').replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        };
    })
;

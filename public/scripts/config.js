'use strict';
// Sets up app and loads modules
angular.module('dealerPortal', ['ui.bootstrap', 'ui.router', 'angularFileUpload', 'toaster']);

/*
 Coding style:
 - Name all functions that will be called, so names are shown on the call stack.
 - Functions that are tied directly to UI interactions and $watch functions does not need to be named.
 - Use descriptive variable names
 - Try to keep line length under 120 characters
 - Use ui-sref in templates to navigate in templates and $state.go elsewhere
 */

angular.module('dealerPortal')

    // All routes goes here
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: '/scripts/dealer/views/login.html',
                controller: 'LoginController'
            })

            .state('files', {
                url: '/files',
                templateUrl: '/scripts/dealer/views/files.html',
                controller: 'FilesController'
            })

            .state('fileUpload', {
                url: '/fileUpload',
                templateUrl: '/scripts/dealer/views/fileUpload.html',
                controller: 'FileUploadController'
            })

            .state('file', {
                url: '/files/{id}',
                templateUrl: '/scripts/dealer/views/file.html',
                controller: 'FileController'
            })

            .state('sales', {
                url: '/sales/{id}',
                templateUrl: '/scripts/dealer/views/sales.html',
                controller: 'SalesController'
            })


            .state('salesProcessed', {
                url: '/sales/{id}/processed',
                templateUrl: '/scripts/dealer/views/sales.html',
                controller: 'SalesProcessedController'
            })

            .state('salesErrors', {
                url: '/sales/{id}/errors',
                templateUrl: '/scripts/dealer/views/sales.html',
                controller: 'SalesErrorsController'
            })

            .state('sale', {
                url: '/sale/{id}',
                templateUrl: '/scripts/dealer/views/sale.html',
                controller: 'SaleController'
            })

            .state('contracts', {
                url: '/contracts',
                templateUrl: '/scripts/dealer/views/contracts.html',
                controller: 'ContractsController'
            })

            .state('contract', {
                url: '/contract/{id}',
                templateUrl: '/scripts/dealer/views/contract.html',
                controller: 'ContractController'
            })

            .state('customers', {
                url: '/customers',
                templateUrl: '/scripts/dealer/views/customers.html',
                controller: 'CustomersController'
            })

            .state('customer', {
                url: '/customers/{id}',
                templateUrl: '/scripts/dealer/views/customer.html',
                controller: 'CustomerController'
            })

            .state('users', {
                url: '/users',
                templateUrl: '/scripts/dealer/views/users.html',
                controller: 'UsersController'
            })

            .state('user', {
                url: '/users/{id}',
                templateUrl: '/scripts/dealer/views/user.html',
                controller: 'UserController'
            })

            .state('stores', {
                url: '/stores',
                templateUrl: '/scripts/dealer/views/stores.html',
                controller: 'StoresController'
            })

            .state('store', {
                url: '/stores/{id}',
                templateUrl: '/scripts/dealer/views/store.html',
                controller: 'StoreController'
            })

            .state('resources', {
                url: '/resources',
                templateUrl: '/scripts/dealer/views/resources.html',
                controller: 'ResourcesController'
            })

            .state('contact', {
                url: '/contact',
                templateUrl: '/scripts/dealer/views/contact.html',
                controller: 'ContactController'
            });

        $urlRouterProvider.otherwise('/');
    })

    // Loads the http request interceptor factory which adds please wait overlay.
    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('httpInterceptor');
    })



;

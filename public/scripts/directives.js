'use strict';

// All directives goes in this file

angular.module('dealerPortal')
    // Makes the close button go back to originating screen for shared screens
    .directive('backButton', ['$window', function backButton ($window) {
        return {
            restrict: 'A',
            link: function (scope, elem) {
                elem.bind('click', function () {
                    $window.history.back();
                    event.preventDefault();
                });
            }
        };
    }])

    // Used for comparing two input fields like password and repeat password
    .directive('compareTo', function compareToDirective () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, model) {
                scope.$watch(attrs.compareTo, function (value) {
                    // Only compare values if the second field has a value.
                    if (model.$viewValue !== undefined && model.$viewValue !== '') {
                        model.$setValidity('compareTo', value === model.$viewValue);
                    }
                });
                model.$parsers.push(function (value) {
                    // Mute the error if the second ctrl is empty.
                    if (value === undefined || value === '') {
                        model.$setValidity('compareTo', true);
                        return value;
                    }
                    var isValid = value === scope.$eval(attrs.compareTo);
                    model.$setValidity('compareTo', isValid);
                    return isValid ? value : undefined;
                });
            }
        };
    })

    // Formats the output in currency fields
    .directive('currencyMask', function currencyMaskDirective () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModelController) {
                // Run formatting on keyup
                var numberWithCommas = function (value, addExtraZero) {
                    if (addExtraZero === undefined) {
                        addExtraZero = false;
                    }
                    value = value.toString();
                    value = value.replace(/[^0-9\.]/g, '');
                    var parts = value.split('.');
                    parts[0] = parts[0].replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, '$&,');
                    if (parts[1] && parts[1].length > 2) {
                        parts[1] = parts[1].substring(0, 2);
                    }
                    if (addExtraZero && parts[1] && (parts[1].length === 1)) {
                        parts[1] = parts[1] + '0';
                    }
                    return parts.join('.');
                };
                var applyFormatting = function () {
                    var value = element.val();
                    var original = value;
                    if (!value || value.length === 0) {
                        return;
                    }
                    value = numberWithCommas(value);
                    if (value !== original) {
                        element.val(value);
                        element.triggerHandler('input');
                    }
                };
                element.bind('keyup', function (e) {
                    var keycode = e.keyCode;
                    var isTextInputKey =
                        (keycode > 47 && keycode < 58) || // number keys
                        keycode === 32 || keycode === 8 || // spacebar or backspace
                        (keycode > 64 && keycode < 91) || // letter keys
                        (keycode > 95 && keycode < 112) || // numpad keys
                        (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
                        (keycode > 218 && keycode < 223);   // [\]' (in order)
                    if (isTextInputKey) {
                        applyFormatting();
                    }
                });
                ngModelController.$parsers.push(function (value) {
                    if (!value || value.length === 0) {
                        return value;
                    }
                    value = value.toString();
                    value = value.replace(/[^0-9\.]/g, '');
                    return value;
                });
                ngModelController.$formatters.push(function (value) {
                    if (!value || value.length === 0) {
                        return value;
                    }
                    value = numberWithCommas(value, true);
                    return value;
                });
            }
        };
    })

    // Sets the date field to short format when using the date picker
    .directive('datepickerPopup', function datepickerPopupDirective (dateFilter, datepickerPopupConfig) {
        return {
            restrict: 'A',
            priority: 1,
            require: 'ngModel',
            link: function (scope, element, attr, ngModel) {
                var dateFormat = attr.datepickerPopup || datepickerPopupConfig.datepickerPopup;
                ngModel.$formatters.push(function (value) {
                    return dateFilter(value, dateFormat);
                });
            }
        };
    })
;
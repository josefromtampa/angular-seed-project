## System setup

* Install node and npm
* Install bower - *npm install -g bower*
* Install dependencies - *npm install* <-- installs package.json
* Install bower dependencies - *bower install*
* Run **node server** to start the webserver

You need to run *npm update* and *bower update* to get the latest version of the modules used by the app when *bower.json* and *package.json* are updated.

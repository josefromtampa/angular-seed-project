    var express = require('express');
    var app = express();

    app.configure(function () {
        app.use(express.compress());
        app.use(express.static(__dirname + '/public', { maxAge: 0 })); 
        app.use(express.logger('dev')); 
    });


    var serverPort = 80;
	   app.listen(serverPort, function () {
        console.log( "Listening on port: " + serverPort );
    });


    